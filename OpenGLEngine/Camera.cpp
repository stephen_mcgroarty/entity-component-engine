#include "Camera.h"



Camera::Camera(): m_Eye(0.0f),m_Target(0.0f,0.0f,-1.0f),m_Up(0.0f,1.0f,0.0f),m_Right(1.0f,0.0f,0.0f),m_Fovy(45.0f)
{}

Camera::Camera(const Vector3 & eye,const Vector3 & target,const Vector3 &up,float fov,float aspect,float near,float far)
	:m_Eye(eye),
	m_Target(target),
	m_Up(up),
	m_Fovy(fov),
	m_AspectRatio(aspect),
	m_Near(near),
	m_Far(far)
{
	m_Right = glm::cross( glm::normalize(m_Eye - m_Target),up);
}

Mat4 Camera::LookAt() const
{
	return glm::lookAt(m_Eye,m_Target,m_Up);
}

void Camera::MoveForward(float distance)
{
	Vector3 at = glm::normalize(m_Eye - m_Target) ;
	m_Eye += at*distance;
	m_Target += at*distance;
}

void Camera::MoveRight(float distance)
{
	m_Eye += m_Right*distance;
	m_Target += m_Right*distance;
}

void Camera::MoveUp(float distance)
{
	m_Eye += m_Up*distance;
	m_Target += m_Up*distance;
}

void Camera::PanCamera(float distance)
{
	Vector3 at = glm::normalize(m_Eye - m_Target) ;
	Vector3 forward = glm::cross(m_Up,glm::cross(at,m_Up));

	m_Eye += forward * distance;
	m_Target += forward*distance;
}
