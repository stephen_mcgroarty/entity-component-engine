#ifndef GAME_ENTITY_H
#define GAME_ENTITY_H
#include "Common.h"
#include <map>
#include "Transform.h"
#include "GameComponent.h"
#include "PrefabFactory.h"

class GameEntity
{
public:
	GameEntity():m_WorldSpace() { }
	GameEntity(const Transform & world):m_WorldSpace(world) { }
	GameEntity(const PrefabFactory::Prefab & prefab);
	virtual ~GameEntity();
	virtual void Update();

	Vector3 GetPosition() const { return m_WorldSpace.GetPosition(); }
	Vector3 GetScale() const    { return m_WorldSpace.GetScale();    }


	//Non const, user should be able to edit the value
	Transform &GetTransform() { return m_WorldSpace; }
	Mat4 GetWorldSpace() const { return m_WorldSpace.ToMatrix(); }
	void SetPostion(const Vector3 & pos){ m_WorldSpace.SetPosition(pos);}
	void SetScale(const Vector3 &scale) { m_WorldSpace.SetScale(scale); }

	void SetId(unsigned int id) { m_Id =   id;}
	unsigned int GetID() const  { return m_Id;}

	virtual void Add(GameComponent * gc);

	GameComponent * GetComponent(const String &name) const;
	GameComponent * GetFamilyComponent(const String &familyName) const;
	const std::map<String,GameComponent*> & GetComponents() const { return m_Components; }
protected:
	unsigned int m_Id; //Unique identifier for this object
	Transform m_WorldSpace;//Entity's transform to world space, including scale and rotation
	std::map<String,GameComponent*> m_Components; //A list of all the game components managed by this object
};

#endif