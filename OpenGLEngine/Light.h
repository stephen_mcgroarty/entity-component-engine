#ifndef LIGHT_H
#define LIGHT_H
#include "Common.h"
#include "GameComponent.h"

class Light: public GameComponent
{
public:
	enum Type
	{
		AMBIENT,
		POINT, 
		DIRECTIONAL
	};

	Light(Type type): m_Type(type){}	
	Light():m_Type(DIRECTIONAL) {} 

	~Light(){}

	void Update(){}
	const String GetID()       const { return "Light";}
	const String GetFamilyID() const { return "Light"; }



	void SetDirection(const Vector3 &dir);
	void SetIntensity(const Vector3 &in)  { m_Intensity = in ; }
	void SetAttentuation(float att)       { m_Attenuation=att; }

	Vector4 GetDirection()const{ return m_Direction;  }
	Vector3 GetIntensity()const{ return m_Intensity;  }
	float GetAttenuation()const{ return m_Attenuation;}

private:	
	
	Type m_Type; 
	Vector4 m_Direction;//w = 1 if point light
	Vector3 m_Intensity;
	float m_Attenuation;
};

#endif