#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H
#include "Collider.h"

class BoundingBox: public Collider
{
public:

	
	const Vector3 & GetDimentions()const { return m_Dimentions;}

	void OnAdd(GameEntity * parent);
	Vector3 GetCollisionNormal(const Collider*) const;


	const String GetID()	   const {return "AABB";}

	void RecalculateMesh();
private:
	Vector3 m_Dimentions;
protected:
	bool Collision(const CircleCollider *) const ;
	bool Collision(const BoundingBox * ) const ;
};

#endif