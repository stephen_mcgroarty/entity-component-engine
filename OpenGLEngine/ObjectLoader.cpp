#include "ObjectLoader.h"
#include <fstream>
#include <sstream>
#include <iostream>




ObjectLoader::ObjectLoader() { }
ObjectLoader::~ObjectLoader() { } 


void ObjectLoader::removeSlashes(std::string & s) const
{
	while(s.find("/") != std::string::npos)
			s.replace(s.find("/"),1," ");
}


FaceFormat ObjectLoader::handleFace(const std::string& txt) const
{
	if( txt.find("/") == std::string::npos )
		return V_ONLY;
	if( txt.find("//") != std::string::npos)
		return VN;
	if( txt.find("/",txt.find("/")+1) != std::string::npos)
		return VTN;

	return VT;

}

/*
std::vector<Vector3> ObjectLoader::generateNormals(const std::vector<Vector3> & verts) const
{
	int index = 0;

	std::vector<Vector3> normals;
	for(unsigned int i =0; i < verts.size(); i+=9)
	{
		Vector3 p1(verts[i],verts[i+1],verts[i+2]);
		Vector3 p2(verts[i+3],verts[i+4],verts[i+5]);

		Vector3 result = glm::cross(p1,p2);
		result = glm::normalize(result);

		normals.push_back(Vector3());
		normals.back().x = result.x;
		normals.back().y = result.y;
		normals.back().z = result.z;
	}

	return normals;

}*/

Mesh * ObjectLoader::load(const std::string & filePath) const
{
	// load file into stream for efficency
	std::ifstream file(filePath,std::ios::binary | std::ios::beg | std::ios::in);
	std::stringstream stream;

	stream << file.rdbuf(); //dump the .txt stream into a string stream

	file.close();

	std::vector<Vector3> verts,normals; //create temp varibles to store the data
	std::vector<Vector2> uv;
	std::vector<unsigned short> indices,uvIndices,normalIndices;

	FaceFormat format = FORMAT_NULL;

	for(std::string str = ""; stream.good(); stream >> str )
	{
		if( str.compare("") == 0 ) continue;


		if( str[0] == 'v') //if the first char is v it will either be reading uvs,normals or vertices
		{
			if(str.size() > 1 && str[1] == 't' ) // uv coords
			{
				float u,v;
				stream >> u >> v;
				uv.push_back(Vector2());
				uv.back().x = u;
				uv.back().y = v;
			}
			else if(str.size() > 1 && str[1] == 'n') // normals
			{
				float x,y,z;
				stream >> x >> y >> z;
				normals.push_back(Vector3());
				normals.back().x = x;
				normals.back().y = y;
				normals.back().z = z;
				
			}
			else // vertices
			{
				float x,y,z;
				stream >> x >> y >> z;
				verts.push_back(Vector3());
				verts.back().x = x;
				verts.back().y = y;
				verts.back().z = z;			
			}
		}
		else if( str[0] == 'f' )
		{
			std::string tmp,t2,t3;
			std::stringstream buffer;
			stream >> tmp >> t2 >> t3;

			if( format == FORMAT_NULL) 
				format = handleFace(tmp);

			removeSlashes(tmp); removeSlashes(t2); removeSlashes(t3);

			buffer << tmp << " "<< t2 << " " << t3;


			if( format == VTN )
			{
				while( buffer.good() )
				{
					unsigned int index[3];
					buffer >> index[0] >> index[1] >> index[2];
					indices.push_back(index[0]-1);
					uvIndices.push_back(index[1]-1);
					normalIndices.push_back(index[2]-1);
				}

					
			} else if( format == V_ONLY )
			{
						
				while (buffer.good())
				{
					unsigned int i;
					buffer >> i;
					indices.push_back(i-1);
				}
			} else if( format == VN )
			{
				while( buffer.good() )
				{
					unsigned int index[2];
					buffer >> index[0] >> index[1];
					indices.push_back(index[0]-1);
					normalIndices.push_back(index[1]-1);
				}
			} else if( format == VT )
			{
				while( buffer.good() )
				{
					unsigned int index[2];
					buffer >> index[0] >> index[1];
					indices.push_back(index[0]-1);
					uvIndices.push_back(index[1]-1);
				}
			}

		}
		else 
		{
			char ch[256];
			stream.getline(ch,256);
		}

	}
	

	if( verts.size() == 0) return NULL; // if no verts could be read



	if(indices.size() == 0) // if the object is not indexed, just return the data
		return new Mesh("Teapot",verts,normals,uv);	

	/*
	*	Otherwise reorder the data
	*/

	int index =0;

	std::vector<Vector3> outVerts,outNorms;
	std::vector<Vector2> outUv;
	std::vector<unsigned short> outIndices;

	for(unsigned int i =0; i < indices.size(); i++)
	{
		unsigned int vertexIndice = indices[i];
		unsigned int normalIndice;
		unsigned int textIndice;

		if( normalIndices.size() != 0 ) normalIndice = normalIndices[i]; else normalIndice =0;
		if( uvIndices.size() != 0 ) textIndice = uvIndices[i];  else textIndice= 0;


		outVerts.push_back( verts[vertexIndice]);

		
		if( normalIndices.size() > normalIndice )
			outNorms.push_back( normals[normalIndice]  );
		


		if( uvIndices.size())
			outUv.push_back( uv[textIndice]);
		

		outIndices.push_back( index++);
	}

//	if( outNorms.size() == 0 )	outNorms = generateNormals(outVerts); //if no normals, generate		TODO: need to test this



	std::vector<Vector3> colour;

	for( unsigned int i =0; i < outVerts.size(); i++)
	{
		colour.push_back(Vector3(1.0f,0.0f,0.0f));
	}


	String name = filePath;

	name.erase(name.find(".obj"));

	std::cout << name << std::endl;

	return new Mesh(name,outVerts,outNorms,outUv,colour);


}