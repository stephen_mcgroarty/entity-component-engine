#include "Collider.h"
#include "BoundingBox.h"
#include "CircleCollider.h"



bool Collider::Collision(const Collider * collider) const
{
	if( dynamic_cast<const BoundingBox*>(collider) != nullptr )
		return Collision(static_cast<const BoundingBox*>(collider));
	
	return Collision(static_cast<const CircleCollider*>(collider));

}
