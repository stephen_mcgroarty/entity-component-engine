#ifndef MESH_RENDERER_H
#define MESH_RENDERER_H
#include "Renderer.h"
#include "Material.h"
#include "BufferObject.h"
#include "VertexArray.h"
#include <vector>

class MeshRenderer:public Renderer
{
public:
	MeshRenderer(Material * m);
	MeshRenderer(const MeshRenderer & copy);

	void Render(const GameScene * scene ) const;
	Material * GetMaterial() const { return m_pMaterial; }

	void OnAdd(GameEntity * parent);
	
	const String GetID() const { return String("Mesh Renderer"); }
private:
	Material * m_pMaterial;


	void InitBuffers();
};

#endif 