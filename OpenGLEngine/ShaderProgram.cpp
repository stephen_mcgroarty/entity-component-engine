#include "ShaderProgram.h"
#include <iostream>
#include <GL\glew.h>
#include <sstream>

ShaderProgram::ShaderProgram(const String& name, Flags flags): m_ProgramName(name),m_Flags(flags)
{
	Init();
}

ShaderProgram::ShaderProgram(const String & name,const String & vertPath,const String & fragPath,Flags flags): m_ProgramName(name),m_Flags(flags)
{

	Init(); //Create program
	m_Flags |= HANDLE_CLEANUP;//*Always* handle the clean up of the shaders

	Shader * vertex = new Shader(Shader::VERTEX),*fragment = new Shader(Shader::FRAGMENT);
	vertex->Load(vertPath);
	fragment->Load(fragPath);
	vertex->Compile();
	fragment->Compile();

	AttachShader(vertex);
	AttachShader(fragment);
	Link();
	
}

void ShaderProgram::Init()
{
	m_Id = glCreateProgram();
	m_Active = m_Linked = false;
	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
		std::cout << "Shader program: " << m_ProgramName << " encountered error: " << err << " whilst creating shader program" << std::endl;
}

ShaderProgram::~ShaderProgram()
{

	if( m_Flags & HANDLE_CLEANUP )
	{
		for(unsigned int iShader =0; iShader < m_vShaders.size(); ++iShader)
		{
			if( m_vShaders[iShader] ) 
			{
				delete m_vShaders[iShader];
				m_vShaders[iShader] = NULL;
			}
		}
		m_vShaders.clear();
	}
	glDeleteProgram(m_Id);
}



void ShaderProgram::AttachShader(Shader * shader)
{
	m_vShaders.push_back(shader);
	glAttachShader(m_Id,shader->GetHandle());

	std::stringstream stream(shader->GetSource());


	bool commented = false; //if code has been commented out with a /* */
	while(stream.good()) 
	{
		std::string temp;
		stream >>temp;
		if( temp.find("//") != -1)
		{
			char ch[256];
			stream.getline(ch,256);
			continue;
		}

		
		if( !commented && temp.find("/*") != -1)
			commented = true;
		if( commented && temp.find("*/") != -1)
			commented = false;
		

		if( temp.compare("uniform") == 0 && !commented)
		{
			std::string type,name;
			stream >> type >> name;
			if( name[name.size()-1] == ';' )
				name.erase(name.end()-1);

			m_Uniforms.insert( std::pair<std::string,std::string>(name,type) );
			std::cout << "uniform " << type << " " << name << std::endl;
		}
	}

	
}



GLuint ShaderProgram::GetID()
{
	return m_Id;
}




void ShaderProgram::Use()
{
	//if( m_Active ) return;
	glUseProgram(m_Id);
	//m_Active = true;
}



void ShaderProgram::Link()
{
	glLinkProgram(m_Id);

	GLenum err = glGetError(); // some error checking

	if( err != GL_NO_ERROR )
	{	
		std:: cout << "Problem linking:\n " << err << std::endl;
		int length;
		glGetShaderiv(m_Id,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(m_Id,length,NULL,ch);
		std::cout << ch << std::endl;
		m_Linked = false; 
		return;
	}

	m_Linked = true;
}



GLuint ShaderProgram::GetUniformLocation(const String & paramName) const 
{
	GLuint loc = glGetUniformLocation(m_Id,paramName.c_str());

	if( loc == -1)
		std::cout << "No uniform found with name: " << paramName << std::endl;

	return loc;
}

void ShaderProgram::SetUniform1i(const String & paramName,const int i) const
{
	glUniform1i(GetUniformLocation(paramName),i);
}

void ShaderProgram::SetUniform1f(const String & str,float f) const
{
	glUniform1f(GetUniformLocation(str),f);
}

void ShaderProgram::SetUniform2fv(const String & str,int count,float *f) const
{
	glUniform2fv(GetUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::SetUniform3fv(const String &str,int count,float *f) const
{
	glUniform3fv(GetUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::SetUniform4fv(const String &str,int count,float *f) const
{
	glUniform4fv(GetUniformLocation(str.c_str()),count,f);
}

void ShaderProgram::SetUniformMatrix3fv(const String &str,int count,bool t,float * value) const
{
	glUniformMatrix4fv(GetUniformLocation(str.c_str()),count,t,value);
}

void ShaderProgram::SetUniformMatrix4fv(const String &str,int count,bool t,float * value) const
{
	glUniformMatrix4fv(GetUniformLocation(str.c_str()),count,t,value);
}
