#include "Ray.h"
#include <GL\glew.h>
#include "GameEntity.h"
#include <iostream>

bool Ray::CheckCollision(const Collider *collider) const
{
	if( collider->GetID().compare("AABB") == 0 )
		return CheckCollision(static_cast<const BoundingBox*>(collider));

	return CheckCollision(static_cast<const CircleCollider*>(collider));	
}

bool Ray::CheckCollision(const BoundingBox * AABB) const
{
	return false;
}




/*
 t^2 (P1.P1) + 2tP1.(P1-c) + (Po - c).(Po - c) - r^2 = 0
 
 OR
 
 A(t^2) + Bt + c = 0
 A = (P1.P1)
 B = 2tP1.(P1-c)
 C = (Po -c).(Po - c) - r^2

 P1 = Ray Direction (Vector)
 Po = Ray origin (Vector)
 t  = Distance along ray (float)
 c  = Sphere position  (Vector) (A.K.A Position)
 r  = radius  (float)
*/


/*
      -B � sqrt(B^2 - 4AC)
 t = ----------------------
                2A
 
t^2 (P1.P1) + 2tP1.(P1-c) + (Po - c).(Po - c8) - r^2 = 0

*/

bool Ray::CheckCollision(const CircleCollider *circle)const
{

	Vector3 center = circle->GetParent()->GetPosition();
	Vector3 toCenter = m_Origin - center;
	//Ray direction
	float B = 2*glm::dot(m_Direction,(toCenter));
	float C = glm::dot(toCenter,toCenter) - (circle->GetRadius()*circle->GetRadius());

	float discriminant = B*B - 4*C;//A is disrigared as it always equals one

	if( discriminant < 0 ) return false;
	if( discriminant == 0) return (-B/2.0f) > 0.0f; // case tangent

	return ((-B - sqrt(discriminant))/2.0f) > 0; 
}


