#ifndef RAY_H
#define RAY_H
#include "GameComponent.h"
#include "Collider.h"
#include "BoundingBox.h"
#include "CircleCollider.h"

class Ray: public GameComponent
{
public:

	Ray(Vector3 direction,Vector3 origin): m_Direction(direction),m_Origin(origin) {}
	~Ray(){}

	const Vector3 & GetDirection() const { return m_Direction; }
	const Vector3 & GetOrigin() const	 { return m_Origin;  }

	void Update() {}
	const String GetFamilyID() const {return "Ray";}
	const String GetID()	   const {return "Ray";}
	bool CheckCollision(const Collider *collider) const;
private:
	Vector3 m_Direction,m_Origin;
	
	bool CheckCollision(const BoundingBox *) const;
	bool CheckCollision(const CircleCollider *)const;
};

#endif