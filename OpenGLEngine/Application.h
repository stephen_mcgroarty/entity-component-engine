#ifndef APPLICATION_H
#define APPLICATION_H
#include <SDL.h>
#include "Common.h"
//If in debug mode, activate the console
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "GameScene.h"
/*
 * Basic mask for the main game loop also initilizes OpenGL and SDL 
 */
class Application
{
public:
	Application(const String & windowName, const unsigned int width,const unsigned int height);
	~Application();

	void Run();
	bool IsRunning() const { return m_Running; }

private:
	bool m_Running;
	void Update();
	void Render();
	void CheckInput();

	

	void InitSDL(const String &); // Inits the SDL window and the OpenGL context
	void InitOpenGL();
	SDL_GLContext m_Context;
	SDL_Window * m_pWindow; 

	GameScene m_GameScene;
	
	unsigned int m_Width,m_Height;//Width and height of the window
	
	Application() {} 

	void LoadPool();
	void LoadStrategic();
};

#endif