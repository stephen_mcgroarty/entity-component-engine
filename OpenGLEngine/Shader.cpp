#include "Shader.h"
#include <fstream>
#include <iostream>
#include "GL\glew.h"


Shader::Shader(ShaderType t): m_Loaded(false),m_Compiled(false),m_Type(t)
{
	if( t == Shader::VERTEX )
		m_Handle = glCreateShader(GL_VERTEX_SHADER);
	else if( t == Shader::FRAGMENT)
		m_Handle = glCreateShader(GL_FRAGMENT_SHADER);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		std::cout << "Error: "  << err << " whilst creating shader" << std::endl;
		int length;
		glGetShaderiv(m_Handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(m_Handle,length,NULL,ch);
		std::cout << ch << std::endl;
		return;
	}
}

Shader::~Shader()
{
	glDeleteShader(m_Handle);
	delete[] m_Source;
}


void Shader::Load(const std::string & filepath)
{
	if( m_Loaded ) return;


	std::ifstream source(filepath.c_str(), std::ios::binary | std::ios::ate| std::ios::in);
	
	GLuint size = source.tellg();
	source.seekg(source.beg);

	char * str = new char[size+1];
	m_Source = str;

	source.read(str,size);
	source.close();
	str[size] = '\0';

	glShaderSource(m_Handle,1,&m_Source,NULL);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		std::cout << "Shader encountered error: " << err << " whilst loading source file: " << filepath << std::endl;
		int length;
		glGetShaderiv(m_Handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(m_Handle,length,NULL,ch);
		std::cout << ch << std::endl;
		
		m_Loaded=false;
		return;
	}


	m_Loaded = true;
}

void Shader::Compile()
{
	if( m_Compiled ) return;
	glCompileShader(m_Handle);


	int comp;
	glGetShaderiv(m_Handle,GL_COMPILE_STATUS,&comp);
	GLenum err = glGetError();
	if( err != GL_NO_ERROR || !comp )
	{
		std::cout << "Shader encountered error: " << err << " whilst compiling source" << std::endl;
		int length;
		glGetShaderiv(m_Handle,GL_INFO_LOG_LENGTH,&length);

		char * ch = new char[length];
		glGetShaderInfoLog(m_Handle,length,NULL,ch);
		std::cout << ch << std::endl;
		m_Compiled = false;
		return;
	}
	m_Compiled = true;
}