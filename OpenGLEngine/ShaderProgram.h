#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H
#include "Shader.h"
#include <vector>
#include <map>
#include "Common.h"


class ShaderProgram
{
public:

	enum Flags
	{
		NONE = 0x0,
		HANDLE_CLEANUP = 0x1
	};

	ShaderProgram(const String& str, Flags flag = NONE);

	//This constructor will *ALWAYS* handle the shader clean up
	ShaderProgram(const String& name,const String &vert,const String & frag,Flags flag = HANDLE_CLEANUP);
	~ShaderProgram();
	String GetName() const { return m_ProgramName; }

	unsigned int GetID();
	void Use();
	void Link();
	void AttachShader(Shader * shader);

	bool IsActive() const { return m_Active; }
	bool IsLinked() const{ return m_Linked; }
	void Deactivate() { m_Active = false; }

	const std::map<String,String> & GetUniforms() const { return m_Uniforms; }

	/*
		Masks for the existing OpenGL calls
	*/
	void SetUniform1i(const String & paramName,const int i) const;
	void SetUniform1f(const String & paramName,const float f) const;
	void SetUniform2fv(const String & paramName, int count, float *f) const;
	void SetUniform3fv(const String & paramName,int count,float *f) const;
	void SetUniform4fv(const String & paramName,int count,float *f) const;
	void SetUniformMatrix3fv(const String & paramName,int count,bool transpose,float *value)const;
	void SetUniformMatrix4fv(const String & paramName,int count,bool transpose,float *value)const;




private:
	std::vector<Shader*> m_vShaders;
	std::string m_ProgramName;


	unsigned int m_Flags;

	unsigned int m_Id;

	unsigned int GetUniformLocation(const std::string & paramName) const;
	void Init();
	std::map<String,String> m_Uniforms;	
	bool m_Active;
	bool m_Linked;
	ShaderProgram(); //so they must have a name
};


#endif