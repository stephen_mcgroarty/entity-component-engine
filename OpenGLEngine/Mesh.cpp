#include "Mesh.h"
#include <iostream>
#include "ResourceManager.h"

bool Mesh::s_SpriteInitilized = false;

Mesh::Mesh(const String & name,const std::vector<Vector3> &vertices,const std::vector<Vector3> & normals ,const std::vector<Vector2> uvs , const std::vector<Vector3> & colours)
	:m_vVertices(vertices),m_vColours(colours),m_vNormals(normals),m_vUV(uvs),m_ID(name)
{
	if( !s_SpriteInitilized )
		InitSprite();
}

Mesh::Mesh(const Mesh & copy):m_vVertices(copy.GetVertices()),m_vColours(copy.GetColours()),m_vNormals(copy.GetNormals()),m_vUV(copy.GetUVs()),m_ID(copy.GetID())
{
	if( !s_SpriteInitilized )
		InitSprite();
}


void Mesh::InitSprite()
{
	s_SpriteInitilized = true;
	
	std::vector<Vector2> uv;
	std::vector<Vector3> verts;
	//Left triangle, strting at bottom left corner
	verts.push_back( Vector3(0.0,0.0,0.0) );//Bottom left
	uv.push_back(Vector2(0.0,0.0) );//bottom left

	verts.push_back( Vector3(1.0,0.0,0.0) );//bottom right
	uv.push_back(Vector2(1.0,0.0) );//bottom right
	
	verts.push_back( Vector3(0.0,1.0,0.0) );//top left
	uv.push_back(Vector2(0.0,1.0) );//top left

	//Right Triangle, starting at top right corner
	verts.push_back( Vector3(1.0,1.0,0.0) );//top right
	uv.push_back(Vector2(1.0,1.0) );//top right of BMP
	
	verts.push_back( Vector3(0.0,1.0,0.0) );//top left
	uv.push_back(Vector2(0.0,1.0) ); //top left
	
	verts.push_back( Vector3(1.0,0.0,0.0) );//bottom right 
	uv.push_back(Vector2(1.0,0.0) );//bottom right

	//No normals
	Mesh * sprite = new Mesh("Sprite",verts,std::vector<Vector3>(),uv);


	ResourceManager::AddMesh(sprite->GetID(),sprite);
}
