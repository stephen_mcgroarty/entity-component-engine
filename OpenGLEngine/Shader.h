#ifndef SHADER_H
#define SHADER_H
#include <string>



class Shader
{
public:
	enum ShaderType
	{
		VERTEX,
		FRAGMENT
	};

	Shader(ShaderType type);
	~Shader();

	void Load(const std::string &);
	void Compile();


	bool IsCompiled()const { return m_Compiled; }
	bool IsLoaded()  const { return m_Loaded; }

	unsigned int GetHandle() const { return m_Handle;}


	const char * GetSource() const { return m_Source; }
private:
	unsigned int m_Handle;
	ShaderType m_Type;
	bool m_Loaded,m_Compiled;
	const char * m_Source;

	Shader() {} 

};

#endif