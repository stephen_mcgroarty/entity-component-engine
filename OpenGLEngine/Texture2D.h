#ifndef TEXTURE2D_H
#define TEXTURE2D_H
#include "GL\glew.h"
#include "Common.h"

class Texture2D
{
public:
	Texture2D():m_Handle(0),m_Height(0),m_Width(0) {}
	Texture2D(const String & filePath);
	enum Target
	{
		TEXTURE0,
		TEXTURE1,
		TEXTURE2,
		TEXTURE3,
		TEXTURE4,
		TEXTURE5,
		TEXTURE6,
		TEXTURE7,
		TEXTURE8,
		TEXTURE9,
	};


	void Bind(Target target) const;
	void Load(const String & filePath);

private:
	unsigned int m_Handle;
	unsigned int m_Height,m_Width;
	GLenum GetTarget(Target target) const;
	static unsigned int s_CurrentlyBoundObject;

};

#endif