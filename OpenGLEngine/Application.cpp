#include "Application.h"
#include <gl\glew.h>
#include <iostream> 
#include "Material.h"
#include "MeshRenderer.h"
#include "Mesh.h"
#include "ObjectLoader.h"
#include "ResourceManager.h"
#include "SpriteRenderer.h"
#include "Tile.h"
#include "BoundingBox.h"
#include "Transform.h"
#include "CircleCollider.h"
#include "RigidBody.h"
#include "ResourceManager.h"
#include "InputManager.h"


Application::Application(const String & windowName, const unsigned int width,const unsigned int height): m_Width(width),m_Height(height)
{
	InitSDL(windowName);
	InitOpenGL();
	m_Running = true;

	ResourceManager::s_Manager.AddProgram(new ShaderProgram("Simple","Simple.vert","Simple.frag"));
	ResourceManager::s_Manager.AddProgram(new ShaderProgram("SimpleTextured","SimpleTexture.vert","SimpleTexture.frag"));
	ResourceManager::s_Manager.AddTexture("Grass",new Texture2D("Grass.bmp"));
	ResourceManager::s_Manager.AddTexture("Water",new Texture2D("Water.bmp"));
	ResourceManager::s_Manager.AddTexture("Dirt",new Texture2D("Dirt.bmp"));
	ResourceManager::s_Manager.AddTexture("Wall",new Texture2D("Wall.bmp"));
	ResourceManager::s_Manager.AddTexture("WallNormal",new Texture2D("WallNormal.bmp"));

	ObjectLoader loader;
	ResourceManager::s_Manager.AddMesh("Sphere",loader.load("Sphere.obj"));
	ResourceManager::s_Manager.AddMesh("Cube",loader.load("cube.obj"));

	
	LoadPool();
	//LoadStrategic();

	Camera *camera = new Camera(Vector3(0.0f,0.0f,60.0f),Vector3(0.0f,0.0f,0.0f),Vector3(0.0f,1.0f,0.0f),45.0f,(float)m_Width/m_Height,1.0f,100.0f);
	m_GameScene.Add(camera);
	Light * light =new Light(Light::DIRECTIONAL);
	light->SetDirection( Vector3(0.0f,1.0f,1.0f) );
	light->SetIntensity( Vector3(0.1f) );
	m_GameScene.Add(light );
}

Application::~Application() 
{}


void Application::LoadPool()
{
	ShaderProgram* program = ResourceManager::s_Manager.GetProgram("Simple");
	ShaderProgram* texturedProgram = ResourceManager::s_Manager.GetProgram("SimpleTextured");

	Tile * t = new Tile();

	RigidBody * r= new RigidBody(1,1,1);
	r->SetVelocity(Vector3(0.0f,0.3f,0.0f));
	
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->Add(new CircleCollider());
	t->Add(r);
	m_GameScene.AddEntity(t);



	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(0.0f,10.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(00.0f,12.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->GetTransform().SetScale(Vector3(1.0f));
	t->SetPostion(Vector3(-2.0f,12.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(2.0f,12.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);


	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(3.0f,14.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(1.0f,14.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(-1.0f,14.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Sphere"));
	t->Add(new MeshRenderer(new Material(program)));	
	t->SetPostion(Vector3(-3.0f,14.0f,0.0f));
	t->Add(new CircleCollider());	
	t->Add(new RigidBody(1,1,2));
	m_GameScene.AddEntity(t);
	

	Material * m = new Material(texturedProgram);
	m->SetTexture(0,"Wall");
	
	t= new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Cube"));
	t->Add(new MeshRenderer(m));
	t->SetScale(Vector3(20.0f,30.0f,1.0f));
	t->SetPostion(Vector3(0.0f,0.0f,-1.0f));
	m_GameScene.AddEntity(t);


	
	m = new Material(texturedProgram);
	m->SetTexture(0,"Wall");
	
	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Cube"));
	t->Add(new MeshRenderer(m));
	t->SetScale(Vector3(20.0f,5.0f,1.0f));
	t->Add(new BoundingBox());	
	t->Add(new RigidBody(1.0,1,2,true));
	t->SetPostion(Vector3(0.0f,30.0f,0.0f));
	m_GameScene.AddEntity(t);


	
	m = new Material(texturedProgram);
	m->SetTexture(0,"Wall");
	
	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Cube"));
	t->Add(new MeshRenderer(m));
	t->SetScale(Vector3(20.0f,5.0f,1.0f));
	t->Add(new BoundingBox());	
	t->Add(new RigidBody(1.0,1,2,true));
	t->SetPostion(Vector3(0.0f,-32.5f,0.0f));
	m_GameScene.AddEntity(t);


	 m = new Material(texturedProgram);
	m->SetTexture(0,"Wall");

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Cube"));
	t->Add(new MeshRenderer(m));
	t->SetScale(Vector3(5.0f,40.0f,1.0f));
	t->Add(new BoundingBox());	
	t->Add(new RigidBody(1.0,1,2,true));
	t->SetPostion(Vector3(20.0f,0.0f,0.0f));
	m_GameScene.AddEntity(t);


	m = new Material(texturedProgram);
	m->SetTexture(0,"Wall");

	t = new Tile();
	t->Add(ResourceManager::s_Manager.GetMesh("Cube"));
	t->Add(new MeshRenderer(m));
	t->SetScale(Vector3(5.0f,40.0f,1.0f));
	t->Add(new BoundingBox());	
	t->Add(new RigidBody(1.0,1,2,true));
	t->SetPostion(Vector3(-20.0f,0.0f,0.0f));
	m_GameScene.AddEntity(t);

}


void Application::LoadStrategic()
{
	ShaderProgram* texturedProgram = ResourceManager::s_Manager.GetProgram("SimpleTextured");

	for(int iTileX = 0; iTileX < 10; iTileX++)
	{
		for(int iTileY = 0; iTileY < 10; iTileY++)
		{
			Tile * t = new Tile();
			t->SetPostion(Vector3(iTileX,iTileY,0.0f));
			t->Add(new Mesh( *ResourceManager::s_Manager.GetMesh("Sprite")));

			Material * material = new Material(texturedProgram);
			material->SetTexture(0,"Grass");
			t->Add(new MeshRenderer(material));
			t->Add(new BoundingBox);
			
			m_GameScene.AddEntity(t);
		}
	}

}

void Application::InitSDL(const String & name)
{
	SDL_Init(SDL_INIT_VIDEO);

	m_pWindow = SDL_CreateWindow(name.c_str(),SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,m_Width,m_Height,SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	m_Context= SDL_GL_CreateContext(m_pWindow);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); 

	SDL_GL_SetSwapInterval(1);

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glEnable(GL_SMOOTH);

	glewExperimental = GL_TRUE;
	GLenum e = glewInit();
	glGetError();
}


void Application::InitOpenGL()
{

}


void Application::Run()
{
	SDL_Event e;
	while( SDL_PollEvent(&e) )
	{
		if( e.type == SDL_QUIT )
			m_Running = false;
		if( e.type & 0x400 ) g_pInput->Update(e);
	}

	CheckInput();
	Update();
	Render();

}


void Application::CheckInput()
{
	m_GameScene.HandleInput();
}

void Application::Update()
{
	m_GameScene.Update();
	const Mouse * m = g_pInput->GetMouse();

}

void Application::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	m_GameScene.RendererScene();

	SDL_GL_SwapWindow(m_pWindow);
}
