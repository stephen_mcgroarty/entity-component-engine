#ifndef COMMON_H
#define COMMON_H
#include <string>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <vector>



typedef std::string String;
typedef unsigned char Byte;

typedef glm::vec4 Vector4;
typedef glm::vec3 Vector3;
typedef glm::vec2 Vector2;
typedef glm::mat4 Mat4;



static float * toFloat(const std::vector<Vector3> & vec)
{
	float * returnee = new float[vec.size()*3];


	unsigned int iReturnee = 0;
	for(unsigned int iVec = 0; iVec < vec.size(); iVec++)
	{
		returnee[iReturnee++] = vec[iVec].x;
		returnee[iReturnee++] = vec[iVec].y;
		returnee[iReturnee++] = vec[iVec].z;
	}

	return returnee;
}


static float * toFloat(const std::vector<Vector2> & vec)
{
	float * returnee = new float[vec.size()*2];
	unsigned int iReturnee = 0; //Index of float to be returned
	for(unsigned int iVec = 0; iVec < vec.size(); iVec++)
	{
		returnee[iReturnee++] = vec[iVec].x;
		returnee[iReturnee++] = vec[iVec].y;
	}

	return returnee;
}


#endif