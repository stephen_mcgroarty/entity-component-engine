#include "GameScene.h"
#include "Renderer.h"
#include "BoundingBox.h"
#include "SDL.h"
#include "CircleCollider.h"
#include "RigidBody.h"
#include "ResourceManager.h"
#include "InputManager.h"


void GameScene::RendererScene() const
{
	for( int iEntity =0; iEntity < m_vEntites.size(); ++iEntity)
	{
		Renderer * renderer = (Renderer*)(m_vEntites[iEntity]->GetFamilyComponent("Renderer"));
		if( renderer )
			renderer->Render(this);
	}


}

void GameScene::Update() 
{
	for(unsigned int iEntity =0; iEntity < m_vEntites.size(); ++iEntity)
	{
		Collider * AABB = static_cast<Collider*>(m_vEntites[iEntity]->GetFamilyComponent("Collider"));
		m_vEntites[iEntity]->Update();		

		if( AABB )
			if( m_pRay->CheckCollision(AABB) )
				std::cout << "Collision with circle: " << iEntity << std::endl;
		


		if( AABB )
		{
			for(unsigned int jEntity =0; jEntity < m_vEntites.size(); ++jEntity)
			{
				if( jEntity == iEntity ) continue;
				Collider * otherAABB = static_cast<Collider*>(m_vEntites[jEntity]->GetFamilyComponent("Collider"));

				if( otherAABB )
					if( AABB->Collision(otherAABB) ) 
					{
						RigidBody * rigidOne = static_cast<RigidBody*>(m_vEntites[iEntity]->GetFamilyComponent("RigidBody"));
						RigidBody * rigidTwo = static_cast<RigidBody*>(m_vEntites[jEntity]->GetFamilyComponent("RigidBody"));

						if( rigidOne && rigidTwo )
						{
							Vector3 collisionNormal = AABB->GetCollisionNormal(otherAABB);
							rigidOne->HandleCollision(rigidTwo,collisionNormal);
						}
					}
			}
		}
	}
}





GameScene::~GameScene()
{
	for( auto iComponent = m_Components.begin(); iComponent != m_Components.end(); ++iComponent)
	{
		delete iComponent->second;
	}
	m_Components.clear();
	
	for( int iEntity =0; iEntity < m_vEntites.size(); ++iEntity)
	{
		delete m_vEntites[iEntity];
	}

}

void GameScene::AddEntity(GameEntity * entity)
{
	m_vEntites.push_back(entity);


	Light * light = (Light*) entity->GetComponent("Light");
	if( light )
		m_vLights.push_back( light );
}


void GameScene::Add(GameComponent * gc)
{
	GameEntity::Add(gc);

	if( gc->GetID().compare("Light") == 0 )
		m_vLights.push_back((Light*)gc);

	if( gc->GetID().compare("Camera") == 0)
	{
		Vector3 at = glm::normalize(static_cast<Camera*>(gc)->GetTarget() - static_cast<Camera*>(gc)->GetEye());
		m_pRay =new Ray(at,static_cast<Camera*>(gc)->GetEye());
	}
}

void GameScene::HandleInput()
{
	Camera * camera = static_cast<Camera*>(m_Components["Camera"]);

	if( camera )
	{
		m_pRay = new Ray(g_pInput->GetMouse()->CastRay(camera));

		const Uint8 * keys = SDL_GetKeyboardState(NULL);

		if( keys[SDL_SCANCODE_A] ) camera->MoveRight(0.1f);
		if( keys[SDL_SCANCODE_D] ) camera->MoveRight(-0.1f);

		if( keys[SDL_SCANCODE_W] ) camera->MoveUp(0.1f);
		if( keys[SDL_SCANCODE_S] ) camera->MoveUp(-0.1f);

		if( keys[SDL_SCANCODE_X] ) camera->MoveForward(0.1f);
		if( keys[SDL_SCANCODE_Z] ) camera->MoveForward(-0.1f);

		if( keys[SDL_SCANCODE_UP] ) m_vEntites[0]->SetPostion( m_vEntites[0]->GetPosition() + Vector3(0.0f,0.1f,0.0f));
		if( keys[SDL_SCANCODE_DOWN] ) m_vEntites[0]->SetPostion( m_vEntites[0]->GetPosition() + Vector3(0.0f,-0.1f,0.0f));

		if( keys[SDL_SCANCODE_RIGHT] ) m_vEntites[0]->SetPostion( m_vEntites[0]->GetPosition() + Vector3(0.1f,0.0f,0.0f));
		if( keys[SDL_SCANCODE_LEFT] ) m_vEntites[0]->SetPostion( m_vEntites[0]->GetPosition() + Vector3(-0.1f,0.0f,0.0f));
	}

}