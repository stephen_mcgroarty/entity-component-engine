#ifndef RIGIDBODY_H
#define RIGIDBODY_H
#include "GameComponent.h"

class RigidBody: public GameComponent
{
public:
	RigidBody(float mass, float friction, float bounce,bool immobile = false): m_Mass(mass), m_Friction(friction), m_Bounce(bounce),m_Velocity(0.0f),m_Immobile(immobile) {} 
	

	void HandleCollision(RigidBody * rigid,Vector3 normal);
	void Update();
	const String GetID()       const {return "RigidBody";}
	const String GetFamilyID() const {return "RigidBody";}
	const Vector3 & GetVelocity() const{ return m_Velocity; }
	float GetMass() const { return m_Mass;}
	float GetBounce()const{ return m_Bounce;}

	void SetVelocity(const Vector3 & vec) { m_Velocity = vec; }
	bool IsImmobile() const {return m_Immobile; }

private:
	float m_Mass;
	float m_Friction;
	float m_Bounce;
	Vector3 m_Velocity;
	bool m_UseGravity;			 //Whether or not the object is affected by gravity
	bool m_AffectedByCollisions; //Whether or not the object ignores collisions
	bool m_Immobile;

};

#endif