#include "ResourceManager.h"
#include <iostream>


std::map<String,Texture2D*> ResourceManager::m_Textures;
std::map<String,Mesh*> ResourceManager::m_Meshes;
std::map<String,ShaderProgram*> ResourceManager::m_Programs;


void ResourceManager::AddTexture(const String & name,Texture2D*texture)
{
	if( m_Textures.find(name) != m_Textures.end() ) return;

	m_Textures[name] = texture;
}


Texture2D* ResourceManager::GetTexture(const String &name)
{
	if( m_Textures.find(name) == m_Textures.end() )
	{
		std::cout << "Error texture with name " << name << " does not exist" << std::endl;
		return nullptr;
	}
	return m_Textures.at(name);	
}


void ResourceManager::AddMesh(const String & name,Mesh*mesh)
{
	if( m_Meshes.find(name) != m_Meshes.end() ) return;
	m_Meshes[name] = mesh;
}

Mesh * ResourceManager::GetMesh(const String &name)
{
	if( m_Meshes.find(name) == m_Meshes.end() )
	{
		std::cout << "Error mesh with name " << name << " does not exist" << std::endl;
		return nullptr;
	}
	return m_Meshes.at(name);	
}


void ResourceManager::AddProgram(ShaderProgram* program)
{
	if( m_Programs.find(program->GetName()) != m_Programs.end() ) return;
	
	m_Programs[program->GetName()] = program;
}

ShaderProgram * ResourceManager::GetProgram(const String & name)
{
	if( m_Programs.find(name) == m_Programs.end() ) 
	{
		std::cout << "Error mesh with name " << name << " does not exist" << std::endl;
		return nullptr;
	}
	return m_Programs[name];	
}
