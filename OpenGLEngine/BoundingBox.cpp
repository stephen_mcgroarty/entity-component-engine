#include "BoundingBox.h"
#include "Mesh.h"
#include "SpriteRenderer.h"
#include "GameEntity.h"
#include "CircleCollider.h"




bool BoundingBox::Collision(const BoundingBox *AABB) const
{
	Vector3 thisPosition = (m_pParent->GetPosition())- (0.5f*m_Dimentions);
	Vector3 ABBBPosition = (AABB->GetParent()->GetPosition() )- (0.5f*AABB->GetDimentions());


	if( thisPosition.x > ABBBPosition.x + AABB->GetDimentions().x)	return false;
	if( thisPosition.x + m_Dimentions.x < ABBBPosition.x)	        return false;

	if( thisPosition.y > ABBBPosition.y + AABB->GetDimentions().y)	return false;
	if( thisPosition.y + m_Dimentions.y < ABBBPosition.y)	        return false;

	if( thisPosition.z > ABBBPosition.z + AABB->GetDimentions().z)	return false;
	if( thisPosition.z + m_Dimentions.z < ABBBPosition.z)	        return false;

	return true;
}


bool BoundingBox::Collision(const CircleCollider *sphere) const
{
	Vector3 thisPosition = (m_pParent->GetPosition())- (0.5f*m_Dimentions);
	Vector3 SpherePosition = (sphere->GetParent()->GetPosition() );


	if( thisPosition.x > SpherePosition.x + sphere->GetRadius())	return false;
	if( thisPosition.x + m_Dimentions.x < SpherePosition.x - sphere->GetRadius())return false;

	if( thisPosition.y > SpherePosition.y + sphere->GetRadius())	return false;
	if( thisPosition.y + m_Dimentions.y < SpherePosition.y - sphere->GetRadius())return false;

	if( thisPosition.z > SpherePosition.z + sphere->GetRadius())	return false;
	if( thisPosition.z + m_Dimentions.z < SpherePosition.z- sphere->GetRadius())return false;

	
	return true;
}


Vector3 BoundingBox::GetCollisionNormal(const Collider * collidee) const
{
	Vector3 displacement = (m_pParent->GetPosition()*m_pParent->GetScale()) - (collidee->GetParent()->GetPosition()*collidee->GetParent()->GetScale());
	return displacement;

}

void BoundingBox::OnAdd(GameEntity * parent)
{
	m_pParent = parent;
	RecalculateMesh();
}


void BoundingBox::RecalculateMesh()
{
	const Mesh* mesh= static_cast<const Mesh*>(m_pParent->GetFamilyComponent("Mesh"));
	if( mesh)
	{
		const std::vector<Vector3>& vertices = mesh->GetVertices();

		if( vertices.size() == 0 ) return;
		
		Vector3 min,max; //minimum x and y and the maximum x and y
		min.x = max.x = vertices[0].x;
		min.y = max.y = vertices[0].y;
		min.z = max.z = vertices[0].z;

		for(unsigned int iVertex = 1; iVertex < vertices.size(); ++iVertex)
		{
			if( vertices[iVertex].x < min.x ) min.x = vertices[iVertex].x; 
			if( vertices[iVertex].y < min.y ) min.y = vertices[iVertex].y;
			if( vertices[iVertex].z < min.z ) min.z = vertices[iVertex].z;

			if( vertices[iVertex].x > max.x ) max.x = vertices[iVertex].x; 
			if( vertices[iVertex].y > max.y ) max.y = vertices[iVertex].y;
			if( vertices[iVertex].z > max.z ) max.z = vertices[iVertex].z;	
		}

		if( min.x < 0 ) min.x = -min.x;//If negitive invert
		if( min.y < 0 ) min.y = -min.y;
		if( min.z < 0 ) min.z = -min.z;

		if( max.x < 0 ) max.x = -max.x;
		if( max.y < 0 ) max.y = -max.y;
		if( max.z < 0 ) max.z = -max.z;
	
		m_Dimentions = (min + max)*m_pParent->GetScale();
	
	}
}
