#ifndef CIRCLE_COLLIDER_H
#define CIRCLE_COLLIDER_H
#include "Collider.h"

class CircleCollider: public Collider
{
public:
	bool Collision(const Vector3 & point)const ;

	void OnAdd(GameEntity * parent);
	const String GetID()	   const {return "CircleCollider";}
	void RecalculateRadius();
	float GetRadius()const {return m_Radius;}

	//Assumes collision has taken place
	Vector3 GetCollisionNormal(const Collider*) const;
	Vector3 GetPenetrationDepth(const CircleCollider*)const;
private:
	float m_Radius;

protected:
	bool Collision(const CircleCollider *) const;
	bool Collision(const BoundingBox * ) const;

};

#endif