#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "Common.h"

class Transform
{
public:

	Transform(): m_Scale(1.0f),m_Position(0.0f) { }
	Vector3 GetPosition() const { return m_Position; }
	Vector3 GetScale()    const { return m_Scale; }

	glm::mat4 ToMatrix() const;
	
	void SetPosition(Vector3 position)  { m_Position = position; }
	void SetScale(Vector3 scale)        { m_Scale = scale; }

private:
	Vector3 m_Position;
	glm::quat m_Rotation;
	Vector3 m_Scale;
};

#endif 