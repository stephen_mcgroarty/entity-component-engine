#include "CircleCollider.h"
#include "Mesh.h"
#include "GameEntity.h"
#include <cmath>
#include "RigidBody.h"
#include <iostream>
#include "BoundingBox.h"

bool CircleCollider::Collision(const Vector3 & point)const 
{

	Vector3 distance = (m_pParent->GetPosition()) - point;
	float length = distance.x*distance.x + distance.y*distance.y + distance.z*distance.z;
	return m_Radius*m_Radius > length;
}


bool CircleCollider::Collision(const BoundingBox * AABB ) const
{
	Vector3 ABBBPosition = (AABB->GetParent()->GetPosition() )- (0.5f*AABB->GetDimentions());
	Vector3 SpherePosition = (m_pParent->GetPosition() );


	if( ABBBPosition.x > SpherePosition.x + m_Radius)	return false;
	if( ABBBPosition.x + AABB->GetDimentions().x < SpherePosition.x - m_Radius)	        return false;

	if( ABBBPosition.y > SpherePosition.y + m_Radius)	return false;
	if( ABBBPosition.y + AABB->GetDimentions().y < SpherePosition.y - m_Radius)	        return false;

	if( ABBBPosition.z > SpherePosition.z + m_Radius)	return false;
	if( ABBBPosition.z + AABB->GetDimentions().z < SpherePosition.z- m_Radius)			return false;

	
	return true;

}


bool CircleCollider::Collision(const CircleCollider * collider) const
{
	//The two radii added together
	float radiiSum = m_Radius + collider->GetRadius();

	//Displacement between the two circles center
	Vector3 displacement =(collider->GetParent()->GetPosition())   -  (m_pParent->GetPosition());
	float distance = glm::length(displacement);
//	if( !thisBody ||otherBody ) //if not dealing with rigid body collisions
	return radiiSum > distance;
	
}


void CircleCollider::OnAdd(GameEntity * parent)
{
	m_pParent = parent;
	RecalculateRadius();
}

void CircleCollider::RecalculateRadius()
{
	const Mesh* mesh= static_cast<const Mesh*>(m_pParent->GetFamilyComponent("Mesh"));
	if( mesh)
	{
		const std::vector<Vector3>& vertices = mesh->GetVertices();

		if( vertices.size() == 0 ) return;
		
		Vector3 right,left; //minimum x, y, z, and the maximum x, y, z
		Vector3 up, down;
		Vector3 near,far;
		right = left = up = down = near = far = vertices[0];

		for(unsigned int iVertex = 1; iVertex < vertices.size(); ++iVertex)
		{
			if( vertices[iVertex].x < left.x ) left = vertices[iVertex]; 
			if( vertices[iVertex].y < down.y ) down = vertices[iVertex];
			if( vertices[iVertex].z < far.z  ) far  = vertices[iVertex];
			 
			if( vertices[iVertex].x > right.x ) right = vertices[iVertex]; 
			if( vertices[iVertex].y > up.y    ) up    = vertices[iVertex];
			if( vertices[iVertex].z > near.z  ) near  = vertices[iVertex];	
		}
		
		
		Vector3 v1,v2;
		if( up.y == right.x && right.x == near.z && down.y == left.x && down.y == far.z) //if a true/complete sphere 
		{
			v1 = down;
			v2 = up;
		}
		else if( (up.y + (-down.y) > right.x + (-left.x) ) 
			&& ( (up.y + (-down.y) > near.z  + (-far.z)) ) ) // if taller than it is wide and taller than it is deep
		{
			v1 = up;
			v2 = down;
		} else if( (near.y + (-far.y) > right.x + (-left.x) ) 
			  && ( (near.y + (-far.y) > (up.y + (-down.y)  )))) // if deeper than it is tall or deep
		{
			v1 = near;
			v2 = far;
		}else 
		{
			v1 = right;
			v2 = left;
		}


		Vector3 distance = (v1-v2)*m_pParent->GetScale();
		float length = distance.x*distance.x + distance.y*distance.y + distance.z*distance.z;

		m_Radius = sqrt( length )/2;
	}

}


Vector3 CircleCollider::GetCollisionNormal(const Collider *circle) const
{
	return (m_pParent->GetPosition()*m_pParent->GetScale()) - (circle->GetParent()->GetPosition()*circle->GetParent()->GetScale());
}


// TO-DO
Vector3 CircleCollider::GetPenetrationDepth(const CircleCollider*circle) const
{
	return Vector3(0.0f);
}
