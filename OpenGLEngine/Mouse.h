#ifndef MOUSE_H
#define MOUSE_H
#include "Common.h"
#include <SDL.h>
#include "Ray.h"
#include "Camera.h"

enum MouseButtonState
{
	BUTTON_STATE_DOWN,
	BUTTON_STATE_UP
};


class Mouse
{
public:
	Mouse();
	~Mouse(){}

	Ray CastRay(const Camera *)const;
	void UpdateEvent(const SDL_Event& e);
	const SDL_Event & GetEvent() const { return m_CurrentEventState; }

	MouseButtonState getRightState() const { return m_Right; }
	MouseButtonState getLeftState()  const { return m_Left;  }
	MouseButtonState getMiddleState()const { return m_Middle;}

	int GetTimeStamp() const { return m_TimeStamp; }
	int GetX() const { return m_X; }
	int GetY() const { return m_Y; }
private:
	SDL_Event m_CurrentEventState;
	int m_X,m_Y;
	unsigned int m_TimeStamp;
	
	MouseButtonState m_Right,m_Left,m_Middle;
	

};



#endif