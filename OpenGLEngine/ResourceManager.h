#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H
#include "Common.h"
#include <map>
#include "Texture2D.h"
#include "ShaderProgram.h"
#include "Mesh.h"

class ResourceManager
{
public:

	static ResourceManager s_Manager;
	
	static void AddTexture(const String & name,Texture2D*texture);
	static Texture2D* GetTexture(const String &name);

	static void AddMesh(const String & name, Mesh *mesh);
	static Mesh* GetMesh(const String & name);

	static void AddProgram(ShaderProgram*);
	static ShaderProgram *GetProgram(const String &);
private:
	static std::map<String,Texture2D*> m_Textures;
	static std::map<String,Mesh*> m_Meshes;
	static std::map<String,ShaderProgram*> m_Programs;
	ResourceManager(){}
};

#endif