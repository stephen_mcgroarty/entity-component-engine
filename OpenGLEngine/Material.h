#ifndef MATERIAL_H
#define MATERIAL_H

#include "GameComponent.h"
#include "ShaderProgram.h"
#include "Camera.h"
#include "Light.h"
#include "GameScene.h"
#include "Texture2D.h"

// This class sends all the data that the shader requires prior to rendering
// Before rendering the object the user should call Bind() to send the uniforms to the shader and bind the texture
class Material: public GameComponent
{
public:
	Material():m_pProgram(0){}
	Material(ShaderProgram * p);
	Material(ShaderProgram * p,Texture2D * tex);
	Material(const Material & material);
	virtual ~Material() {}


	virtual void Update() {}


	

	//This function will 'Bind' all of the properties that the shader requires
	//The camera parameter is the camera which is viewing the object
	void Bind(const GameScene * scene ) const;

	void SetTexture(const String &uniform,const String& textureID);
	void SetTexture(unsigned int textureNumber ,const String& textureID);


	const String GetID()       const {return "Material";}
	const String GetFamilyID() const {return "Material";}

	const ShaderProgram * GetProgram() const { return m_pProgram; }
	void SetProgram(ShaderProgram* p)        { m_pProgram = p;    }
protected:
	ShaderProgram * m_pProgram;


	//Links the textures from the shader to the texture data map
	void LinkTextures();


	//Uniform name first, texture ID to be bound at that location second
	std::map<String,String> m_TextureData;

	Mat4 m_OldModel,m_OldView,m_OldProjection; //Check these to see if the uniforms need updated each time
	Vector4 m_OldLightDir;
	Vector3 m_OldLightIntensity;
};


#endif