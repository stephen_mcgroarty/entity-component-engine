#version 330

layout(location = 0) in vec3 in_Verts;
layout(location = 4) in vec3 in_Colours;
layout(location = 2) in vec3 in_Normals;
layout(location = 1) in vec2 in_UV;


out vec3 out_Colour;
out vec3 out_Normal;
out vec3 eyeCoords;
out vec2 UV;

uniform mat4 MV;
uniform mat4 MVP;

void main()
{
	eyeCoords = (MV*vec4( in_Verts,1.0)).xyz;
	out_Normal = in_Normals;
	out_Colour = in_Colours;
	UV = in_UV;
	gl_Position = MVP* vec4( in_Verts,1.0);
}