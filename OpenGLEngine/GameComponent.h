#ifndef GAME_COMPONENT_H
#define GAME_COMPONENT_H
#include "Common.h"

class GameEntity;

class GameComponent
{
public:

	enum CleanupType
	{
		INSTANCE,
		REFRENCE
	};
	GameComponent():m_CleanupType(INSTANCE) { }
	GameComponent(GameEntity * parent,CleanupType cleanupType = INSTANCE):m_pParent(parent),m_CleanupType(cleanupType) {}
	

	virtual ~GameComponent() {} 
	virtual void Update() = 0;
	virtual const String GetID()       const =0;
	virtual const String GetFamilyID() const =0;
	
	//When added as child of the parent parameter
	virtual void OnAdd(GameEntity * parent) {}
	CleanupType GetCleanupType() const { return m_CleanupType; }


	void SetParent(GameEntity * parent) { m_pParent = parent; }
	const GameEntity * GetParent() const{ return m_pParent;   }


protected:
	// A pointer to the game entity that controls this component
	GameEntity * m_pParent;
	CleanupType m_CleanupType;
};


#endif