#ifndef RENDERER_H
#define RENDERER_H
#include "GameComponent.h"
#include "Camera.h"
#include "GameScene.h"
#include "VertexArray.h"
#include "BufferObject.h"


class Renderer: public GameComponent
{
public:

	Renderer(): m_Loaded(false) { }
	virtual void Update() {}
	virtual void Render(const GameScene * scene ) const = 0;
	virtual const String GetFamilyID() const { return String("Renderer"); }

protected:
	
	struct RenderData
	{
		VertexArray VAO;
		std::vector<BufferObject> buffers;
	};

	static std::map<String,RenderData> s_vRenderables;

	bool m_Loaded;
};

#endif