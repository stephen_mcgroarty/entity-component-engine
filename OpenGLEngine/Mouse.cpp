#include "Mouse.h"
#include <iostream>


void Mouse::UpdateEvent(const SDL_Event &e)
{
	m_CurrentEventState = e;

	m_TimeStamp = e.button.timestamp;
	
	m_X = e.motion.x;
	m_Y = 640 - e.motion.y;

	if( e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_Left = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_LEFT && e.button.type == SDL_MOUSEBUTTONUP )
		m_Left = BUTTON_STATE_UP;
	

	if( e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_Right = BUTTON_STATE_DOWN;
	 else if(e.button.button == SDL_BUTTON_RIGHT && e.button.type == SDL_MOUSEBUTTONUP )
		m_Right = BUTTON_STATE_UP;


	if( e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONDOWN )
		m_Middle = BUTTON_STATE_DOWN;
	else if(e.button.button == SDL_BUTTON_MIDDLE && e.button.type == SDL_MOUSEBUTTONUP )
		m_Middle = BUTTON_STATE_UP;
}

Mouse::Mouse():m_Left(BUTTON_STATE_UP),m_Right(BUTTON_STATE_UP),m_Middle(BUTTON_STATE_UP),m_X(0),m_Y(0)
{}


Ray Mouse::CastRay(const Camera *camera)  const
{	
	// TO-DO
	//Change these values to trickle through from main.cpp
	float width = 800.0f;
	float height= 640.0f;
	
	float aspectRatio = camera->GetAspect();
	

	double screenSpaceX = ((m_X/(width))*2.0f)-1.0f;
	double screenSpaceY = ((m_Y/(height))*2.0f)-1.0f;


	double viewRatio = std::tan(((float) 3.14159265359 / (180.0f/camera->GetFovy()) / 2.00f)); 
	
	screenSpaceX = screenSpaceX * viewRatio;
	screenSpaceY = screenSpaceY * viewRatio;

	
	Vector4 cameraNear = glm::vec4((float)(screenSpaceX * camera->GetNear()), (float)(screenSpaceY * camera->GetNear()), -camera->GetNear(), 1);
	Vector4 cameraFar = glm::vec4((float)(screenSpaceX * camera->GetFar()), (float)(screenSpaceY * camera->GetFar()), -camera->GetFar(), 1);


	Mat4 inverseView = glm::inverse(camera->LookAt());
	Vector4 worldSpaceNear = inverseView * cameraNear;
	Vector4 worldSpaceFar =  inverseView * cameraFar;

	Vector3 rayOrigin = Vector3(worldSpaceNear.x, worldSpaceNear.y, worldSpaceNear.z);
	Vector3 rayDirection = Vector3(worldSpaceFar.x - worldSpaceNear.x, worldSpaceFar.y - worldSpaceNear.y, worldSpaceFar.z - worldSpaceNear.z);
	rayDirection = glm::normalize(rayDirection);

	//std::cout << rayDirection.x << " " << rayDirection.y << " " << rayDirection.z << std::endl;
	return Ray(rayDirection,rayOrigin);
}