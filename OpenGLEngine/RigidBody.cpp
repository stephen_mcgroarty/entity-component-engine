#include "RigidBody.h"
#include "GameEntity.h"


void RigidBody::Update()
{
	if( !m_Immobile )
		m_pParent->SetPostion( m_pParent->GetPosition() + m_Velocity);	
}


void RigidBody::HandleCollision(RigidBody * rigid,Vector3 collisionNormal)
{
	Vector3 velocity1 = m_Velocity;
	Vector3 velocity2 = rigid->GetVelocity();

	collisionNormal = glm::normalize(collisionNormal);	
	Vector3 relitiveVelocity = velocity2 - velocity1;


	float bounce = (m_Bounce + rigid->GetBounce())/2.0f;
	float vDotN = glm::dot(relitiveVelocity,collisionNormal);

	if( vDotN < 0 ) return;
	float j = -(1+bounce) * vDotN;
	j /= (1.0f/m_Mass) + (1.0f/rigid->GetMass());

	Vector3 impulse = collisionNormal*j;

	if( !m_Immobile )
		m_Velocity =  velocity1 - ( (1.0f/m_Mass)*impulse);

	if( !rigid->IsImmobile() )
		rigid->SetVelocity( velocity2 + ( (1.0f/rigid->GetMass())*impulse));
}