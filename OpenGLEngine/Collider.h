#ifndef COLLIDER_H
#define COLLIDER_H
#include "GameComponent.h"


class CircleCollider;
class BoundingBox;

class Collider: public GameComponent
{
public:
	void Update() {}
	const String GetFamilyID() const {return "Collider";}
	virtual const String GetID()const =0;


	bool Collision(const Collider *collider) const;
	virtual Vector3 GetCollisionNormal(const Collider*) const = 0;
protected:
	virtual bool Collision(const CircleCollider *) const = 0;
	virtual bool Collision(const BoundingBox * ) const =0;

};

#endif