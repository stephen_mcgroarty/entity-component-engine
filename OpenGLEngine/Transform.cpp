#include "Transform.h"
#include <iostream>


glm::mat4 Transform::ToMatrix() const
{
	glm::mat4 matrix(1.0f);
	
	matrix[0].x = m_Scale.x;
	matrix[1].y = m_Scale.y;
	matrix[2].z = m_Scale.z;

	matrix[3] = Vector4(m_Position,1.0f);

	/*glm::mat4 test(1.0f);

	test = glm::translate(test,m_Position);
	test = glm::scale(test,m_Scale);
	*/

	return matrix;
}