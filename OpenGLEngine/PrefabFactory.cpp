#include "PrefabFactory.h"
#include <iostream>
#include "GameEntity.h"


// TO-DO initialze else where
PrefabFactory * g_pPrefabFactory = new PrefabFactory();

void PrefabFactory::CreatePrefab(const String & name, GameEntity * prefab)
{
	if( m_Prefabs.find(name) != m_Prefabs.end() )
	{
		std::cout << "Error prefab named: " << name << " already added" << std::endl;
		return;
	}

	m_Prefabs[name].m_Components = prefab->GetComponents();
}

const PrefabFactory::Prefab & PrefabFactory::GetInstance(const String & name) const
{
	return  m_Prefabs.at(name);
}