#include "SpriteRenderer.h"
#include <iostream>



void SpriteRenderer::Render(const GameScene * scene) const 
{
	if( m_Loaded )
	{
		m_pMaterial->Bind(scene);
		m_pSprite->Draw();
		GLenum drawMode = GL_TRIANGLES;
		s_vRenderables[m_pSprite->GetID()].VAO.BindVertexArray();
		glDrawArrays(drawMode,0,m_pSprite->GetVertices().size()*3);
	} 
	else 
		std::cout << "Error in mesh renderer: VAO and VBO's not loaded!" << std::endl;
}

void SpriteRenderer::OnAdd(GameEntity * parent)
{
	m_pParent = parent;
	if( m_pMaterial )
		m_pMaterial->SetParent(parent);
	if( m_pSprite )
		m_pSprite->SetParent(parent);
	
	InitBuffers();
}



void SpriteRenderer::InitBuffers()
{

	String name = m_pSprite->GetID();	

	if(  s_vRenderables.find(name) != s_vRenderables.end() ) //If already loaded
	{
		m_Loaded =true;
		return;
	}


	if( m_pSprite->GetVertices().size() == 0 )
	{
		std::cout << "Error no vertices in sprite" << std::endl;
		return;
	}

	//Add the buffers and VAO in the the list of renderables (to prevent the same sprite being loaded twice)
	s_vRenderables[name] = RenderData();

	m_Loaded = true;
	s_vRenderables[name] .VAO.CreateVertexArray();

	s_vRenderables[name] .buffers.push_back( BufferObject() ); //Vertices
	s_vRenderables[name] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)* m_pSprite->GetVertices().size()*3,toFloat(m_pSprite->GetVertices()));
	s_vRenderables[name] .VAO.VertexArribPointer(0,3,VertexArray::FLOAT);

	if( m_pSprite->GetUVs().size() != 0)
	{
		s_vRenderables[name] .buffers.push_back( BufferObject() ); //UVs
		s_vRenderables[name] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)* m_pSprite->GetUVs().size()*2,toFloat(m_pSprite->GetUVs()));
		s_vRenderables[name] .VAO.VertexArribPointer(1,2,VertexArray::FLOAT);
	}
	
}

