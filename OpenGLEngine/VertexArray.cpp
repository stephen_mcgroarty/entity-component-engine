#include "VertexArray.h"

void VertexArray::CreateVertexArray()
{
	glGenVertexArrays(1,&m_VertexArrayIndex);
	glBindVertexArray(m_VertexArrayIndex);
}


void VertexArray::VertexArribPointer(unsigned int index,unsigned int length, DataType type) const
{	
	GLenum dataType;

	if( type == FLOAT )
		dataType = GL_FLOAT;

	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index,length,dataType,GL_FALSE,0,0);
}

void VertexArray::BindVertexArray() const
{
	glBindVertexArray(m_VertexArrayIndex);
	GLenum error = glGetError();
	if( error != GL_NONE )	std::cout << "Error binding VAO: " << m_VertexArrayIndex << " error code: " << error << std::endl;
}