#version 330

out vec4 colour;

in vec3 out_Colour;
in vec3 out_Normal;
in vec3 eyeCoords;
in vec2 UV;


uniform vec4 LightDir;
uniform vec3 LightIntensity;



vec3 phongShade(vec3 colour)
{
	vec3 v = normalize( -eyeCoords.xyz );
	vec3 s;
	
	if( LightDir.w == 0.0 ) //if directional light
		s = normalize( vec3(LightDir) );
	else 
		s = normalize(vec3(LightDir.xyz - eyeCoords));

	float sDotN = max( dot(s,out_Normal),0.0); 
	vec3 h = normalize( v + s);

	vec3 i = colour*0.1;								   					//ambient
	i += colour* 0.7 * sDotN;					 							//diffuse

	if( sDotN > 0.0 ) i += colour * pow(max( dot(h,out_Normal),0.0),2) * LightIntensity; //specular

	return i;
}

void main()
{
	colour = vec4(phongShade(out_Colour),1.0);

}
