#ifndef MESH_H
#define MESH_H
#include "Common.h"
#include "GameComponent.h"



class Mesh: public GameComponent
{
public:
	Mesh(const String &name,const std::vector<Vector3> &vertices,const std::vector<Vector3> & normals = std::vector<Vector3>(),const std::vector<Vector2> uvs = std::vector<Vector2>(), const std::vector<Vector3> & colours = std::vector<Vector3>());
	Mesh(const Mesh &);


	static void InitSprite();
	void Update() {}
	const String GetFamilyID() const {return "Mesh";}
	const String GetID()	   const {return m_ID;  }

	// Returns the number of vertices in the mesh
	unsigned int GetSize() const { return m_vVertices.size(); }

	//Returns the Normals of each vertex
	const std::vector<Vector3> & GetNormals()  const { return m_vNormals;}
	//Returns the colour of each vertex
	const std::vector<Vector3> & GetColours()  const { return m_vColours;}
	//Returns the UV coordinates of each vertex
	const std::vector<Vector2> & GetUVs()      const { return m_vUV;}	
	//Returns all of the vertex positions in the mesh
	const std::vector<Vector3> & GetVertices() const { return m_vVertices;}


private:
	std::vector<Vector3> m_vNormals;
	std::vector<Vector3> m_vColours;
	std::vector<Vector2> m_vUV;
	std::vector<Vector3> m_vVertices;

	static bool s_SpriteInitilized;

	String m_ID; //Each mesh has its own ID, NOTE Duplicates share the ID!!! 
	Mesh() { }

};

#endif 