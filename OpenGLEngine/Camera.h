#ifndef CAMERA_H
#define CAMERA_H
#include "Common.h"
#include "GameComponent.h"

class Camera: public GameComponent
{
public:
	Camera(); 
	Camera(const Vector3 & eye,const Vector3 & target,const Vector3 &up,float fov,float aspect,float near,float far);


	virtual void Update() {}
	virtual const String GetID()       const { return "Camera"; }
	virtual const String GetFamilyID() const { return "Camera"; }
	

	Mat4 LookAt() const;
	Mat4 GetProjection() const { return glm::perspective(m_Fovy,m_AspectRatio,m_Near,m_Far); }


	Vector3 GetEye()   const { return m_Eye;   }
	Vector3 GetRight() const { return m_Right; }
	Vector3 GetUp()    const { return m_Up;    }
	Vector3 GetTarget()const { return m_Target;}

	float GetFovy()  const { return m_Fovy; }
	float GetAspect()const { return m_AspectRatio;}
	float GetNear()  const { return m_Near; }
	float GetFar()   const { return m_Far; }


	void MoveForward(float distance);
	void MoveUp(float distance);
	void MoveRight(float distance);
	void PanCamera(float distance);
private:
	Vector3 m_Eye,m_Right,m_Up,m_Target;
	float m_Fovy,m_AspectRatio,m_Near,m_Far;
};


#endif