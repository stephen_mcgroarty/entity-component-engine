#ifndef BUFFER_OBJECT_H
#define BUFFER_OBJECT_H
#include <GL\glew.h>

class BufferObject
{
public:
	BufferObject(): m_BufferIndex(0) { }

	enum BufferType
	{
		ARRAY_BUFFER,
		ELEMENT_ARRAY
	};

	enum DrawType
	{
		STATIC,
		DYNAMIC
	};

	template<typename T>
	void CreateBuffer(BufferType type,unsigned int size,T *data);

	void BindBuffer(BufferType type) const;

	GLenum GetBufferType(BufferType type) const;

	unsigned int GetBufferIndex() const { return m_BufferIndex; }
	~BufferObject();
private:
	unsigned int m_BufferIndex;
};


template<typename T>
void BufferObject::CreateBuffer(BufferType type,unsigned int size,T *data)
{
	GLenum bufferType;
	if( type == ARRAY_BUFFER )
		bufferType = GL_ARRAY_BUFFER;
	else if( type == ELEMENT_ARRAY)
		bufferType = GL_ELEMENT_ARRAY_BUFFER;

	glGenBuffers(1,&m_BufferIndex);
	glBindBuffer(GL_ARRAY_BUFFER,m_BufferIndex);
	glBufferData(GL_ARRAY_BUFFER,size,data,GL_STATIC_DRAW);
}






#endif