#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H
#include "Mesh.h"
#include <vector>
#include <string>
#include "Common.h"

// V = vertex, T = texture coordinate, N = Normal 
enum FaceFormat{
	V_ONLY,
	VT,
	VTN,
	VN,
	FORMAT_NULL
};


class ObjectLoader
{
public:

	ObjectLoader();
	~ObjectLoader();

	Mesh * load(const std::string &filePath) const;

private:
	std::vector<Vector3> generateNormals(const std::vector<Vector3> &verts) const;
	void removeSlashes(std::string &) const;

	//returns what format the faces are in
	FaceFormat handleFace(const std::string &) const;

};


#endif