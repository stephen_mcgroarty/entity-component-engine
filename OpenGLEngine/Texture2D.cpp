#include "Texture2D.h"
#include "SDL.h"
#include <iostream>

unsigned int Texture2D::s_CurrentlyBoundObject = 0;

Texture2D::Texture2D(const String & filePath)
{
	Load(filePath);
}


void Texture2D::Load(const String &filePath)
{
	const char * fname = filePath.c_str();
	glGenTextures(1, &m_Handle); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, m_Handle);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}
	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
	externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);


	m_Width = tmpSurface->w;
	m_Height = tmpSurface->h;
	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	
}


void Texture2D::Bind(Target target) const 
{
	if( s_CurrentlyBoundObject != m_Handle)
	{
		glActiveTexture( GetTarget(target) );
		glBindTexture(GL_TEXTURE_2D,m_Handle);
		s_CurrentlyBoundObject = m_Handle;
	}
}


GLenum Texture2D::GetTarget(Target target) const
{
	if( target == TEXTURE0 )
		return GL_TEXTURE0;
	if( target == TEXTURE1 )
		return GL_TEXTURE1;
	if( target == TEXTURE2 )
		return GL_TEXTURE2;
	if( target == TEXTURE3 )
		return GL_TEXTURE3;
	if( target == TEXTURE4 )
		return GL_TEXTURE4;
	if( target == TEXTURE5 )
		return GL_TEXTURE5;
	if( target == TEXTURE6 )
		return GL_TEXTURE6;
	if( target == TEXTURE6 )
		return GL_TEXTURE6;
	if( target == TEXTURE7 )
		return GL_TEXTURE7;
	if( target == TEXTURE8 )
		return GL_TEXTURE8;
	if( target == TEXTURE9 )
		return GL_TEXTURE9;
	return GL_TEXTURE0;
}