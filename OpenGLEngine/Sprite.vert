#version 330

layout(location = 0) in vec3 in_Verts;
layout(location = 1) in vec2 in_UV;

out vec2 UV;

uniform mat4 MVP;

void main()
{
	UV = in_UV;
	gl_Position = MVP* vec4( in_Verts,1.0);
}