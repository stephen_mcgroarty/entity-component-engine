#ifndef SPRITE_RENDERER_H
#define SPRITE_RENDERER_H
#include "Renderer.h"
#include "Sprite.h"
#include "Material.h"

class SpriteRenderer: public Renderer
{
public:

	SpriteRenderer(Sprite * s,Material * m): m_pSprite(s),m_pMaterial(m) { }

	void Render(const GameScene * scene) const;
	const String GetID() const { return String("Sprite Renderer"); }

	void OnAdd(GameEntity * parent);


	void SetSprite(Sprite * sprite) { m_pSprite = sprite; }
	Sprite *GetSprite() const { return m_pSprite; }
private:
	Material * m_pMaterial;
	Sprite *m_pSprite;
	void InitBuffers();
};


#endif