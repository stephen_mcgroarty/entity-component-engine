#include "MeshRenderer.h"
#include "GameEntity.h"
#include "Mesh.h"
#include <iostream>

std::map<String,MeshRenderer::RenderData> MeshRenderer::s_vRenderables = std::map<String,MeshRenderer::RenderData>();


MeshRenderer::MeshRenderer(Material * mat): m_pMaterial(mat)
{}

MeshRenderer::MeshRenderer(const MeshRenderer &copy):m_pMaterial(copy.GetMaterial())
{

}

void MeshRenderer::Render(const GameScene * scene ) const 
{
	if( m_Loaded )
	{				
		m_pMaterial->Bind(scene);
		Mesh * mesh = (Mesh*)(m_pParent->GetFamilyComponent("Mesh")); //No RTTI use C style cast instead
		if( mesh )
		{	
			GLenum drawMode = GL_TRIANGLES;
			s_vRenderables[mesh->GetID()].VAO.BindVertexArray();
			glDrawArrays(drawMode,0,mesh->GetSize()*3);
		}
	} else 
		std::cout << "Error in mesh renderer: VAO and VBO's not loaded!" << std::endl;
}

void MeshRenderer::OnAdd(GameEntity * parent)
{
	m_pParent = parent;
	if( m_pMaterial )
		m_pMaterial->SetParent(parent);

	InitBuffers();
}

void MeshRenderer::InitBuffers()
{
	if( !m_pParent )
	{
		std::cout << "Error in mesh renderer, no parent set!" << std::endl;
		return;
	}

	
	Mesh * mesh = (Mesh*)(m_pParent->GetFamilyComponent("Mesh")); 
	if( mesh )
	{
		if(s_vRenderables.find(mesh->GetID()) == s_vRenderables.end() )
		{
			if( mesh->GetSize() == 0 )
			{
				std::cout << "Error no vertices in mesh" << std::endl;
				return;
			}
			//Add the buffers and VAO in the the list of renderables (to prevent the same mesh being loaded twice)
			s_vRenderables[mesh->GetID()] = RenderData();

			m_Loaded = true;
			s_vRenderables[mesh->GetID()] .VAO.CreateVertexArray();
			s_vRenderables[mesh->GetID()] .buffers.push_back( BufferObject() ); //Vertices
			s_vRenderables[mesh->GetID()] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)* mesh->GetSize()*3,toFloat(mesh->GetVertices()));
			s_vRenderables[mesh->GetID()] .VAO.VertexArribPointer(0,3,VertexArray::FLOAT);

			if( mesh->GetColours().size() != 0)
			{
				s_vRenderables[mesh->GetID()] .buffers.push_back( BufferObject() ); //Colours
				s_vRenderables[mesh->GetID()] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)* mesh->GetColours().size()*3,toFloat( mesh->GetColours()));
				s_vRenderables[mesh->GetID()] .VAO.VertexArribPointer(4,3,VertexArray::FLOAT);
			}

			if( mesh->GetNormals().size() != 0)
			{
				s_vRenderables[mesh->GetID()] .buffers.push_back( BufferObject() ); //Normals
				s_vRenderables[mesh->GetID()] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)*  mesh->GetNormals().size()*3,toFloat(mesh->GetNormals()));
				s_vRenderables[mesh->GetID()] .VAO.VertexArribPointer(2,3,VertexArray::FLOAT);
			}

			if( mesh->GetUVs().size() != 0)
			{
				s_vRenderables[mesh->GetID()] .buffers.push_back( BufferObject() ); //UVs
				s_vRenderables[mesh->GetID()] .buffers.back().CreateBuffer(BufferObject::ARRAY_BUFFER,sizeof(float)* mesh->GetUVs().size()*2,toFloat(mesh->GetUVs()));
				s_vRenderables[mesh->GetID()] .VAO.VertexArribPointer(1,2,VertexArray::FLOAT);
			}


		} else 
			m_Loaded = true;
	}else 
	{
		std::cout << "Error in mesh renderer: parent has no mesh!" << std::endl;

	}
}
