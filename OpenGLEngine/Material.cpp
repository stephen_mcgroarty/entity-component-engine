#include "Material.h"
#include <iostream>
#include "GameEntity.h"
#include "GameScene.h"
#include "ResourceManager.h"

Material::Material(const Material & material):m_pProgram(material.m_pProgram),m_TextureData(material.m_TextureData)
{
}

void Material::Bind(const GameScene * scene ) const
{
	if( m_pProgram == NULL )
	{
		std::cout << "Error in material: No shader set" << std::endl;
		return;
	}

	m_pProgram->Use();
	const std::map<String,String> uniforms = m_pProgram->GetUniforms();


	Camera *camera = (Camera *)scene->GetFamilyComponent("Camera");

	if( !camera )
	{
		std::cout << "Scene has no camera!" << std::endl;
		return;
	}

	Light * light = (Light*)scene->GetFamilyComponent("Light");

	Mat4 model      = m_pParent->GetWorldSpace();
	Mat4 view       = camera->LookAt();
	Mat4 projection = camera->GetProjection();


	//These varibles are used to determine whether or not to send the data to the GPU
	bool modelChanged = model == m_OldModel ? false : true;
	bool viewChanged  = view == m_OldView ? false : true;
	bool projectionChanged = projection == m_OldProjection ? false : true;
	
	bool lightDirectionChanged = false;
	bool lightIntChanged = false;
	
	int count =0;

	for(std::map<String,String>::const_iterator iTextures = m_TextureData.begin(); iTextures != m_TextureData.end(); ++iTextures,++count)
	{
		if( iTextures->second.compare("NULL") != 0)
		{
			m_pProgram->SetUniform1i(iTextures->first,count);
			ResourceManager::s_Manager.GetTexture(iTextures->second)->Bind(static_cast<Texture2D::Target>(Texture2D::TEXTURE0+count));
		}	
	}

	
	if( light )
	{
		lightDirectionChanged = light->GetDirection() == m_OldLightDir ? false : true;
		lightIntChanged = light->GetIntensity() == m_OldLightIntensity? false : true;
	}


	/* Set the model, view and projection varibles */
	if( modelChanged )
	{
		//Model uniform AKA position in world
		if( uniforms.find("model") != uniforms.end() && uniforms.find("model")->second.compare("mat4") == 0 )
			m_pProgram->SetUniformMatrix4fv("model",1,false,glm::value_ptr(model));
		else if( uniforms.find("M") != uniforms.end() && uniforms.find("M")->second.compare("mat4") == 0 )
			m_pProgram->SetUniformMatrix4fv("M",1,false,glm::value_ptr(model));
	}

	if( viewChanged )
	{
		//View uniform
		if( uniforms.find("view") != uniforms.end() && uniforms.find("view")->second.compare("mat4") == 0)
			m_pProgram->SetUniformMatrix4fv("view",1,false,glm::value_ptr(view));
		else if( uniforms.find("V") != uniforms.end() && uniforms.find("V")->second.compare("mat4") == 0)
			m_pProgram->SetUniformMatrix4fv("V",1,false,glm::value_ptr(view));
	}

	if( projectionChanged )
	{
		//Projection uniform
		if( uniforms.find("projection") != uniforms.end() && uniforms.find("projection")->second.compare("mat4") == 0)
			m_pProgram->SetUniformMatrix4fv("projection",1,false,glm::value_ptr(view));
		else if( uniforms.find("P") != uniforms.end() && uniforms.find("P")->second.compare("mat4") == 0)
			m_pProgram->SetUniformMatrix4fv("P",1,false,glm::value_ptr(view));
	}

	//Merged forms (for less GPU computations)
	if( modelChanged || viewChanged )
	{
		if( uniforms.find("MV") != uniforms.end() && uniforms.find("MV")->second.compare("mat4") == 0)
		{
			Mat4 MV = view*model;
			m_pProgram->SetUniformMatrix4fv("MV",1,false,glm::value_ptr(MV));
		}
	}
	if( modelChanged || viewChanged || projectionChanged )
	{
		if( uniforms.find("MVP") != uniforms.end() && uniforms.find("MVP")->second.compare("mat4") == 0)
		{
			Mat4 MVP = projection*view*model;
			m_pProgram->SetUniformMatrix4fv("MVP",1,false,glm::value_ptr(MVP));
		}
	}
	/* End of MVP stuff */

	/* Lighting stuff */
	if( lightDirectionChanged )
	{
		if( uniforms.find("LightDir") != uniforms.end() && uniforms.find("LightDir")->second.compare("vec4") == 0)
		{
			Vector4 dir = light->GetDirection();
			m_pProgram->SetUniform4fv("LightDir",1,glm::value_ptr(dir));
		}
	}

	if( lightIntChanged )
	{
		if( uniforms.find("LightIntensity") != uniforms.end() && uniforms.find("LightIntensity")->second.compare("vec3") == 0)
		{
			Vector3 dir = light->GetIntensity();
			m_pProgram->SetUniform3fv("LightIntensity",1,glm::value_ptr(dir));
		}
	}
	/* End of Lighting stuff */
}


void Material::SetTexture(unsigned int textureNumber ,const String& textureID)
{
	if( textureNumber < m_TextureData.size() )
	{
		auto itr = m_TextureData.begin();

		for(unsigned int i = 0; i < textureNumber; i++)
		{
			++itr;
		}
		
		itr->second = textureID;
	}
}


void Material::SetTexture(const String & uniform,const String & texture)
{
	if( m_TextureData.find(uniform) == m_TextureData.end() )
		m_TextureData[uniform] = texture;
}

void Material::LinkTextures()
{
	for(auto iUniform = m_pProgram->GetUniforms().begin(); iUniform != m_pProgram->GetUniforms().end(); ++iUniform)
	{
		if( iUniform->second.compare("sampler2D") == 0 )
			m_TextureData[iUniform->first] = "NULL";			
		
	}
}

Material::Material(ShaderProgram * p): m_pProgram(p),m_OldLightDir(Vector4(0.0f)),m_OldLightIntensity(Vector4(0.0f))
{ 
	LinkTextures();
}


Material::Material(ShaderProgram * p,Texture2D * tex):m_pProgram(p),m_OldLightDir(Vector4(0.0f)),m_OldLightIntensity(Vector4(0.0f)) 
{
	LinkTextures();
}