#ifndef SPRITE_H
#define SPRITE_H
#include "Texture2D.h"
#include "GameComponent.h"

class Sprite: public GameComponent
{
public:
	Sprite();
	Sprite(const String& texture):m_Texture(texture) { Init(); }
	//Binds the texture
	void Draw() const;


	
	void Update() {}
	const String GetFamilyID() const {return "Sprite";}
	const String GetID()	   const {return "Sprite";}//Should only be one set of rendering data, so make all sprites have the same ID


	//Returns the UV coordinates of each vertex
	const std::vector<Vector2> & GetUVs()      const { return m_vUV;}	
	//Returns all of the vertex positions in the mesh
	const std::vector<Vector3> & GetVertices() const { return m_vVertices;}

private:
	String m_Texture;
	std::vector<Vector2> m_vUV;
	std::vector<Vector3> m_vVertices;

	void Init();

};


#endif