#include "Light.h"

void Light::SetDirection(const Vector3 &dir)
{
	float w = m_Type == POINT ? ((m_Type == AMBIENT) ? -1.0f: 0.0f) : 0.0f;
	m_Direction = Vector4(dir,w);
}
