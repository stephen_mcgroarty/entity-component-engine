#include "BufferObject.h"



void BufferObject::BindBuffer(BufferType type ) const
{
	GLenum bufferType = GetBufferType(type);
	glBindBuffer(bufferType,m_BufferIndex);
}


GLenum BufferObject::GetBufferType(BufferType type) const
{
	if( type == ARRAY_BUFFER )
		return GL_ARRAY_BUFFER;
	else if( type == ELEMENT_ARRAY)
		return GL_ELEMENT_ARRAY_BUFFER;
	return GL_FALSE; 
}

BufferObject::~BufferObject() 
{ 
	//glDeleteBuffers(1,&m_BufferIndex); 
}
