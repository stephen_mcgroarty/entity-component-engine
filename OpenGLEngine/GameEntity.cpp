#include "GameEntity.h"


GameEntity::GameEntity(const PrefabFactory::Prefab & prefab)
{
	for(auto itr = prefab.m_Components.begin(); prefab.m_Components.end() != itr; ++itr)
	{
	//	if( itr->second->GetCleanupType() == GameComponent::INSTANCE )
	//		m_Components[itr->first] = new GameComponent()
	}
}

void GameEntity::Add(GameComponent * gc)
{
	if( m_Components.find(gc->GetID()) == m_Components.end() )
	{
		m_Components.insert(std::pair<String,GameComponent*>(gc->GetID(),gc)); 
		gc->SetParent(this);
		gc->OnAdd(this);
	}
}

GameEntity::~GameEntity()
{
	for(auto itr = m_Components.begin(); m_Components.end() != itr; ++itr)
	{
//		if( itr->second )
//			delete itr->second;			
	}
	m_Components.clear();
}

GameComponent * GameEntity::GetComponent(const String &name) const
{
	std::map<String,GameComponent*>::const_iterator cItr =  m_Components.find(name);

	if( cItr == m_Components.end() ) return nullptr;
	return cItr->second;
}

GameComponent * GameEntity::GetFamilyComponent(const String &familyName) const
{
	for(std::map<String,GameComponent*>::const_iterator cItr =  m_Components.begin(); cItr != m_Components.end(); ++cItr)
	{
		if( cItr->second->GetFamilyID().compare(familyName) == 0)
			return cItr->second;
	}
	return nullptr;
}


void GameEntity::Update()
{
	for(auto iChild = m_Components.begin(); iChild != m_Components.end(); ++iChild)
	{
		iChild->second->Update();
	}
}