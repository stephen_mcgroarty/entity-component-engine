#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H
#include <GL\glew.h>
#include <iostream>

class VertexArray
{
public:

	VertexArray(): m_VertexArrayIndex(0) { }

	~VertexArray() { glDeleteVertexArrays(1,&m_VertexArrayIndex); }

	enum DataType
	{
		FLOAT
	};

	void CreateVertexArray();
	
	void BindVertexArray() const;
	void VertexArribPointer(unsigned int index,unsigned int length,DataType type) const;

	unsigned int GetVertexArrayIndex() const { return m_VertexArrayIndex; }

private:
	unsigned int m_VertexArrayIndex;

};






#endif