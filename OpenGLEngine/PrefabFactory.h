#ifndef PREFAB_FACTORY_H
#define PREFAB_FACTORY_H
#include <map>
#include "Common.h"



class GameEntity;
class GameComponent;


class PrefabFactory
{
public:
	struct Prefab
	{
		std::map<String,GameComponent*> m_Components;
	};
	PrefabFactory(){}
	~PrefabFactory();


	//The GameEntity will have it's  with a string to identify it
	void CreatePrefab(const String & name, GameEntity * prefab);

	//Get a instance of a previously added GameEntity prefab
	const Prefab & GetInstance(const String & name) const;

private:
	std::map<String,Prefab> m_Prefabs;
	
};


extern PrefabFactory * g_pPrefabFactory;

#endif