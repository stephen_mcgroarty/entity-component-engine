#ifndef INPUT_H
#define INPUT_H
#include "Mouse.h"

class InputManager
{
public:
	InputManager() {m_pMouse = new Mouse();}
	~InputManager(){}

	void Update(const SDL_Event &e) { m_pMouse->UpdateEvent(e); }
	const Mouse *GetMouse() const { return m_pMouse;}

private:
	Mouse * m_pMouse;
};

extern InputManager * g_pInput;

#endif