#ifndef GAME_SCENE_H
#define GAME_SCENE_H
#include "GameEntity.h"
#include <vector>
#include "Camera.h"
#include "Light.h"
#include "Ray.h"
class GameScene: public GameEntity
{
public:
	~GameScene();
	void RendererScene() const;
	void Update();

	void AddEntity(GameEntity * entity);


	//Override the Add component method of base class, point to lights in children
	void Add(GameComponent *gc);
	
	void HandleInput();

private:
	std::vector<GameEntity*> m_vEntites;
	std::vector<Light*> m_vLights;
	Ray* m_pRay;
};

#endif