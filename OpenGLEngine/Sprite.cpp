#include "Sprite.h"
#include "ResourceManager.h"

Sprite::Sprite(): m_vVertices(std::vector<Vector3>()),m_vUV(std::vector<Vector2>())
{
	Init();
}

void Sprite::Init()
{
	//Left triangle, strting at bottom left corner
	m_vVertices.push_back( Vector3(0.0,0.0,0.0) );//Bottom left
	m_vUV.push_back(Vector2(0.0,0.0) );//bottom left

	m_vVertices.push_back( Vector3(1.0,0.0,0.0) );//bottom right
	m_vUV.push_back(Vector2(1.0,0.0) );//bottom right
	
	m_vVertices.push_back( Vector3(0.0,1.0,0.0) );//top left
	m_vUV.push_back(Vector2(0.0,1.0) );//top left

	//Right Triangle, starting at top right corner
	m_vVertices.push_back( Vector3(1.0,1.0,0.0) );//top right
	m_vUV.push_back(Vector2(1.0,0.0) );//top right of BMP
	
	m_vVertices.push_back( Vector3(0.0,1.0,0.0) );//top left
	m_vUV.push_back(Vector2(0.0,1.0) ); //top left
	
	m_vVertices.push_back( Vector3(1.0,0.0,0.0) );//bottom right 
	m_vUV.push_back(Vector2(1.0,1.0) );//bottom right
}

void Sprite::Draw() const
{
	ResourceManager::s_Manager.GetTexture(m_Texture)->Bind(Texture2D::TEXTURE0);
}