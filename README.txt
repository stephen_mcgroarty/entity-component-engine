This was a small project of mine to experiment with different engine architectures NOT the accuracy of the physics system! Basicly 
everything on screen is of the class “Tile: public GameEntity { };”. All the other features are added as a component like, Rendering, 
physics, collisions ect. Performance is quite good but when compared like for like with another engine I wrote is very poor, I believe
 this to be due to inherently poor cache locality across the components. Especially the rendering data!
